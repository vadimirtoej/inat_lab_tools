#ifndef MEMORYBLOCK_H
#define MEMORYBLOCK_H

#include <cstddef>
#include <cstdint>
#include <string>
#include <type_traits>
#include <vector>

// Класс представляет блок байт в памяти, куда можно удобно записывать данные,
// формируя пакеты любой структуры, и считывать данные, разбирая пакеты.
class MemoryBlock final
{
public:
    // Создание блока нулевого или заранее известного размера.
    // В любом случае размер блока увеличивается по мере надобности.
    MemoryBlock() = default;
    MemoryBlock(size_t size);

    // Получение данных блока и их размера.
    const char* getData() const { return bytes_.data(); }
          char* getData()       { return bytes_.data(); }
    size_t      getSize() const { return bytes_.size(); }

    // Увеличивает размер блока на `by' байт.
    void growBy(size_t by);

    // Универсальное добавление данных любого простого типа в блок.
    template<typename T>
    void append(const T& data);

    // Добавление в блок данных нескольких распространенных типов.
    void appendByte   (uint8_t byte);
    void appendNatural(uint32_t natural);
    void appendString (const std::string& string);

    // Универсальное считывание данных любого простого типа из блока.
    // Возвращается считанное значение, начинавшееся на байте блока,
    // заданном position, а position увеличивается на размер значения.
    template<typename T>
    const T& read(size_t& position) const;

    // Считывание из блока заначений нескольких распространенных типов.
    uint8_t     readByte   (size_t& position) const;
    uint32_t    readNatural(size_t& position) const;
    const char* readString (size_t& position) const;

private:
    std::vector<char> bytes_;
};


// Ниже находятся фрагменты реализации.

template<typename T>
void MemoryBlock::append(const T& data)
{
    static_assert(
        std::is_pod<T>::value,
        "Допускаются только простые типы (не указатели, не классы).");

    const auto offset = bytes_.size();
    bytes_.resize(offset + sizeof(T));
    *reinterpret_cast<T*>(bytes_.data() + offset) = data;
}

template<typename T>
const T& MemoryBlock::read(size_t& position) const
{
    static_assert(
        std::is_pod<T>::value,
        "Допускаются только простые типы (не указатели, не классы).");

    size_t const offset = position;
    position += sizeof(T);
    return *reinterpret_cast<const T*>(bytes_.data() + offset);
}


#endif // MEMORYBLOCK_H
