#include "MemoryBlock.h"

#include <cstring>

MemoryBlock::MemoryBlock(size_t size) :
    bytes_(size)
{
}

void MemoryBlock::growBy(size_t by)
{
    bytes_.resize(bytes_.size() + by);
}

void MemoryBlock::appendByte(uint8_t byte)
{
    append(byte);
}

void MemoryBlock::appendNatural(uint32_t natural)
{
    append(natural);
}

void MemoryBlock::appendString(const std::string& string)
{
    const auto offset = bytes_.size();
    bytes_.resize(offset + string.size() + sizeof(std::string::value_type));
    std::strcpy(bytes_.data() + offset, string.c_str());
}

uint8_t MemoryBlock::readByte(size_t& position) const
{
    return read<uint8_t>(position);
}

uint32_t MemoryBlock::readNatural(size_t& position) const
{
    return read<uint32_t>(position);
}

const char* MemoryBlock::readString(size_t& position) const
{
    const char* string = bytes_.data() + position;
    position += std::strlen(string);
    position += sizeof(char);
    return string;
}
