#include "networking.h"

#include <windows.h>

int NET_initialize()
{
    WSADATA wsa;
    return ::WSAStartup(MAKEWORD(2, 2), &wsa);
}

int NET_finalize()
{
    return ::WSACleanup();
}

int NET_get_last_error()
{
    return ::WSAGetLastError();
}

const char* NET_get_error_message(int code)
{
    static char* buffer = nullptr;
    if (buffer) {
        ::LocalFree(buffer);
    }
    DWORD const result = ::FormatMessage(
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
            nullptr,
            code,
            0,
            (LPTSTR)&buffer,
            0,
            nullptr);
    if (result) {
        ::CharToOemBuff(buffer, buffer, ::strlen(buffer));
        return buffer;
    }
    return "<error description not found>";
}

bool NET_is_error(int result)
{
    return result == SOCKET_ERROR;
}

bool NET_is_invalid(SOCKET socket)
{
    return socket == INVALID_SOCKET;
}
