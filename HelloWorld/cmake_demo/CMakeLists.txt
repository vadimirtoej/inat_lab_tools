#минимальная версия cmake, которая нам подойдёт
cmake_minimum_required(VERSION 2.6)

#задаём имя проекту
project(hello_world)

#заносим в переменную CMAKE_CXX_FLAGS необходимые флаги для компилятора
set(CMAKE_CXX_FLAGS "-std=c++11 -Wall")

#заносим в переменную SOURCES файлы *.cpp, из которых мы будем компилировать проект
set(SOURCES
	main.cpp
)

#осуществляем поиск нужных пакетов. В рамках данного примера это дейтсвие неактуально
#в качестве примера пытаемся найти библиотеку boost
#find_package(Boost 1.55.0 REQUIRED)

#здесь можно задать директории, в которых будет осуществляться поиск заголовочных файлов
include_directories(
)

#здесь можно указать директории, в которых будет осуществляться поиск
#библиотек для линковки
link_directories(${CMAKE_CURRENT_SOURCE_DIR})

add_executable(${PROJECT_NAME} ${SOURCES})

#выполняем линоковку библиотек. В рамках данного примера это действие неактуально
target_link_libraries(${PROJECT_NAME})
